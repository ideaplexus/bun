ARG PROXY_CACHE_PREFIX
ARG BUN_VERSION=latest

FROM ${PROXY_CACHE_PREFIX}oven/bun:${BUN_VERSION} AS bun

FROM ${PROXY_CACHE_PREFIX}node:lts

ARG BUN_VERSION=latest

ENV BUN_VERSION=${BUN_VERSION}

RUN groupadd bun \
      --gid 1001 \
    && useradd bun \
      --uid 1001 \
      --gid bun \
      --shell /bin/sh \
      --create-home

COPY --from=bun /usr/local/bin/docker-entrypoint.sh /usr/local/bin
COPY --from=bun /usr/local/bin/bun /usr/local/bin
COPY --from=bun /usr/local/bin/bunx /usr/local/bin

# smoke test
RUN set -eux \
    git --version \
    node --version \
    npm --version \
    bun --version

WORKDIR /home/bun/app
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD ["/usr/local/bin/bun"]