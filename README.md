# Bun container

A Container with `Bun` and with `Node` (LTS) as fallback.
You can get this container by running `docker pull ideaplexus/bun`.