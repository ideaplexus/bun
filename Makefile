#!make

BUILD_CACHE_PREFIX="ideaplexus/"
PROXY_CACHE_PREFIX=""
BUN_VERSION="1.1"

.PHONY: lint
lint:
	docker run --rm -i -v ${PWD}/.hadolint.yml:/bin/hadolint.yml -e XDG_CONFIG_HOME=/bin hadolint/hadolint:latest-alpine < Dockerfile

.PHONY: build
build:
	docker build --file Dockerfile --tag $(BUILD_CACHE_PREFIX)bun --build-arg "PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX)" --build-arg "BUN_VERSION=$(BUN_VERSION)" .

.PHONY: all
all: lint build